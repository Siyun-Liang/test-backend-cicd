package com.example.testbackendcicd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestBackendCicdApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestBackendCicdApplication.class, args);
    }

}
